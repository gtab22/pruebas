import unittest

def plusone(x):
    return x + 1

def divideby2(x):
    return x / 2.0

class MyTestCase(unittest.TestCase):
    def test_plusone(self):
        self.assertEqual(plusone(3), 4)

    def test_divideby2(self):
        self.assertEqual(divideby2(5), 2.5)
